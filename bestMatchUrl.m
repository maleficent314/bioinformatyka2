%function through which user can enter data by its id
function [] = bestMatchUrl(id1,id2,mode,match,mismatch,gap)

firstFasta=fromURL(id1);
secondFasta=fromURL(id2);
name1=id1;name2=id2;
seq1=firstFasta.sequence;seq2=secondFasta.sequence;

bestMatch(name1,seq1,name2,seq2,mode,match,mismatch,gap);

end

