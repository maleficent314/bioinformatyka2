% function returns a table filled out with prizes and the final score 
% for mode similarity
function [table,score] = similarity(seq1,seq2,match,sub,indel)
n=length(seq1);m=length(seq2);
table=zeros(n+1,m+1);

for i=1:(n+1)
    for j=1:(m+1)
        
       if (i==1 && j>1)
           table(i,j)=table(i,j-1)+indel;
       elseif  (j==1 && i>1)
            table(i,j)=table(i-1,j)+indel;   
       elseif (i>1 && j>1)
           
           if seq1(i-1)==seq2(j-1)
               table(i,j)=table(i-1,j-1)+match;
           else
               
           A=[table(i-1,j-1) table(i,j-1) table(i-1,j)];
           a=max(A);  
            if a==table(i-1,j-1)
            table(i,j)=a+sub; 
            else
            table(i,j)=a+indel;
            end
            
           end
           
       end

    end
end
score=table(n+1,m+1);
end

