%function returns sequence in structure found by id
function [structure] = fromURL(id)

URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi';
fastaContent = webread(URL, "db","nucleotide","rettype","fasta","id",id);
fastaContent=strsplit(fastaContent,'\n');
n=length(fastaContent);

sequence='';
for i=1:n
    if ~(char(fastaContent(1,i))=='>')
    sequence=strcat(sequence,fastaContent(1,i));
    end
end

structure.sequence=char(sequence);

end