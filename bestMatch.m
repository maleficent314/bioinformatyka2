%function through which user can enter data by files names
function [] = bestMatch(name1,seq1,name2,seq2,mode,match,mismatch,gap)

if mode=="distance"
[table,score]=distance(seq1,seq2,match,mismatch,gap);
else
    [table,score]=similarity(seq1,seq2,match,mismatch,gap);
end
[aln1,aln2,alnLength,nMatch,nGap,matchConnection,path]=optimalPath(table,seq1,seq2,mismatch,gap);
saveStatistics(name1,name2,mode,match,mismatch,gap,score,alnLength,nMatch,nGap,aln1,aln2,matchConnection);
graphicBoard(table,name1,name2,path);

end
