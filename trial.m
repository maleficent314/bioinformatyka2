%% example 1
bestMatchUrl('AB355691.1','JN800419.1','similarity',1,-0.5,-1)
%% example 2
bestMatchFile('HipposiderosCommersoni.fasta','RhinolophusCapensis.fasta','similarity',1,-0.5,-1)
%% example 3
bestMatch('ncbi1','GCAGTC','ncbi2','CTAGG','distance',0,1,1)
%% example 4
bestMatchFile('HipposiderosCommersoni.fasta','RhinolophusCapensis.fasta','distance',0,1,1)
%% example 5
bestMatchUrl('AB355691.1','XM_015544310.1','similarity',1,-0.5,-1)