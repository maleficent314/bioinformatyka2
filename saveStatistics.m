%function saves statistics about sequences and their matching
function [] = saveStatistics(name1,name2,mode,match,mismatch,gap,score,length,nMatch,nGap,aln1,aln2,matchConnection)
fid = fopen('Statistics', 'a');
fprintf(fid, ['\n# 1: ' name1 '\n# 2: ' name2 '\n# Mode: ' mode] );
fprintf(fid,'\n# Match: %.1f\n# Mismatch: %.1f\n# Gap: %.1f\n# Score: %.1f',match,mismatch,gap,score);
fprintf(fid,'\n# Length: %d',length);
fprintf(fid,'\n# Identity: %d/%d (%.2f%%)',nMatch,length,(nMatch/length*100));
fprintf(fid,'\n# Gaps: %d/%d (%.2f%%)',nGap,length,(nGap/length*100));
fprintf(fid,'\n%s\n%s\n%s',aln1,matchConnection,aln2);
fclose(fid);
end

