%function through which user can enter data manually
function [] = bestMatchFile(file1,file2,mode,match,mismatch,gap)

firstFasta=fromFile(file1);
secondFasta=fromFile(file2);
name1=file1;name2=file2;
seq1=firstFasta.sequence;seq2=secondFasta.sequence;

bestMatch(name1,seq1,name2,seq2,mode,match,mismatch,gap);

end

