%function returns sequence in structure found by fasta file name
function [structure] = fromFile(file)

fastaContent = fileread(file);
fastaContent=strsplit(fastaContent,'\n');
n=length(fastaContent);

sequence='';
for i=1:n
    if ~(char(fastaContent(1,i))=='>')
    sequence=strcat(sequence,fastaContent(1,i));
    end
end
structure.sequence=char(sequence);

end

