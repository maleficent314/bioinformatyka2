% function presents in graphic form table filled with costs or prizes 
% also shows on the same figure the most optimal path

function [] = graphicBoard(table,name1,name2,path)

figure('Name','The most optimal path.');
imagesc(table);
title('Graphic display of the board');
xlabel(name2);ylabel(name1);
colorbar;
hold on;
x=[];y=[];
[ySize,xSize] = size(path);

for i=1:ySize
    for j=1:xSize
        
        if path(i,j)==1
            x = [x j];
            y = [y i];
        end
        
    end
end
plot(x,y,'k');
saveas(gcf,'Board.png');

end

