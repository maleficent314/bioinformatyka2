% function returns table with the most optimal path, alignments and series
% of parameters: length of alignments, amount of matches and gaps, match
% connection to visualise similarity of 2 sequences
function [aln1,aln2,alnLength,nMatch,nGap,matchConnection,path] = optimalPath(table,seq1,seq2,sub,indel)

n=length(seq1);m=length(seq2);
aln1="";aln2="";matchConnection="";
i=n+1;j=m+1;
nMatch=0;nGap=0;
path=zeros(i,j);path(i,j)=1;

while (i>0 && j>0)
        
        if (i==1 && j==1)
            break;
        elseif (i>1 && j>1 && seq1(i-1)==seq2(j-1))
            aln1=seq1(i-1)+aln1;
            aln2=seq2(j-1)+aln2;
            path(i,j)=1;
            i=i-1;j=j-1;nMatch=nMatch+1;
            matchConnection="|" + matchConnection; 
        elseif (i>1 && j>1 && (table(i,j)==table(i-1,j-1)+sub) && (seq1(i-1)~=seq2(j-1)))
            aln1=seq1(i-1)+aln1;
            aln2=seq2(j-1)+aln2;
            path(i-1,j-1)=1;
            i=i-1;j=j-1;
            matchConnection=" " + matchConnection;
        elseif (j>1 && (table(i,j)==table(i,j-1)+indel))
            aln1="-"+aln1;
            aln2=seq2(j-1)+aln2;
            path(i,j-1)=1;
            j=j-1;nGap=nGap+1;
            matchConnection=" " + matchConnection;
            
        else
            aln1=seq1(i-1)+aln1;
            aln2="-"+aln2;
            path(i-1,j)=1;
            i=i-1;nGap=nGap+1;
            matchConnection=" " + matchConnection;
            
        end
        
        alnLength=strlength(aln1); 
        
end

